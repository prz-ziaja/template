orgnize-dirs:
	mkdir -p data/mlflow_minio data/mlflow_postgres data/tensorboard

run-mlflow:
	docker-compose -f mlflow/mlflow_compose.yaml --env-file mlflow/mlflow_config.env up -d --build
stop-mlflow:
	docker-compose -f mlflow/mlflow_compose.yaml --env-file mlflow/mlflow_config.env stop

run-tensorboard:
	docker-compose -f tensorboard/tensorboard_compose.yaml up -d
stop-tensorboard:
	docker-compose -f tensorboard/tensorboard_compose.yaml down

run-monitoring-tools:
	run-mlflow
	run-tensorboard

stop-monitoring-tools:
	stop-mlflow
	stop-tensorboard