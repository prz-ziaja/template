from ray.train.lightning import (
    RayDDPStrategy,
    RayLightningEnvironment,
    RayTrainReportCallback,
    prepare_trainer,
)
import pytorch_lightning as pl
from ray import tune
from ray.tune.schedulers import ASHAScheduler
from ray.train.torch import TorchTrainer, TorchConfig
from ray.train import RunConfig, ScalingConfig, CheckpointConfig
from ray.air.integrations.mlflow import MLflowLoggerCallback, setup_mlflow
import importlib
import mlflow
from ray.tune import Trainable
from ray import train


def build_train_func(source_module):
    def train_func(config):
        dm = source_module.HMSDataModule(batch_size=config["batch_size"])
        model = source_module.HMSClassifier(config)

        setup_mlflow(
            config,
            experiment_name="Default",
            #ctx.get_experiment_name()   ctx.get_local_world_size()  ctx.get_node_rank()         ctx.get_trial_dir()         ctx.get_trial_name()        ctx.get_world_rank()
            #ctx.get_local_rank()        ctx.get_metadata()          ctx.get_storage()           ctx.get_trial_id()          ctx.get_trial_resources()   ctx.get_world_size()
            run_name=train.get_context().get_trial_name(),
            tracking_uri="http://127.0.0.1:5000",
            tags={"trial_dir":train.get_context().get_trial_dir()}
        )
        mlflow.pytorch.autolog()

        trainer = pl.Trainer(
            devices="auto",
            accelerator="auto",
            strategy=RayDDPStrategy(),
            callbacks=[RayTrainReportCallback()],
            plugins=[RayLightningEnvironment()],
            enable_progress_bar=False,
        )
        trainer = prepare_trainer(trainer)
        trainer.fit(model, datamodule=dm)
    return train_func


def tune_hms_asha(ray_trainer, search_space, num_epochs, num_samples=10):
    scheduler = ASHAScheduler(max_t=num_epochs, grace_period=1, reduction_factor=2)

    tuner = tune.Tuner(
        ray_trainer,
        param_space={"train_loop_config": search_space},
        tune_config=tune.TuneConfig(
            metric="ptl/val_kl",
            mode="min",
            num_samples=num_samples,
            scheduler=scheduler,
        ),
    )
    return tuner.fit()

def main(module_name):
    search_space_registry = importlib.import_module(f"models.search_spaces")
    search_space = search_space_registry.__getattribute__(module_name)
    source_module = importlib.import_module(f"models.{module_name}")

    mlflow.set_tracking_uri("http://127.0.0.1:5000")
    mlflow.set_experiment("Default")

    train_func = build_train_func(source_module)

    # The maximum training epochs
    num_epochs = search_space_registry.__getattribute__("max_num_epochs")

    # Number of sampls from parameter space
    num_samples = search_space_registry.__getattribute__("max_num_samples")

    scaling_config = ScalingConfig(
        num_workers=1,
        resources_per_worker={"GPU": 0.3},
        use_gpu=True, 
    )

    run_config = RunConfig(
        checkpoint_config=CheckpointConfig(
            num_to_keep=2,
            checkpoint_score_attribute="ptl/val_kl",
            checkpoint_score_order="min",
        ),
    )

    # Define a TorchTrainer without hyper-parameters for Tuner
    ray_trainer = TorchTrainer(
        train_func,
        scaling_config=scaling_config,
        run_config=run_config,
        torch_config=TorchConfig(backend="gloo")
    )
    results = tune_hms_asha(ray_trainer, search_space, num_epochs, num_samples=num_samples)
    return results


if __name__ == "__main__":
    main("simple_cnn_arch")
    