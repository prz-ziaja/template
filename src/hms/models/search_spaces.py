from ray import tune

max_num_epochs = 16
max_num_samples = 10

simple_cnn_arch = {
    "layer_1_size": tune.choice([32,64,128]),
    "layer_2_size": tune.choice([32,64,128]),
    "lr": tune.loguniform(1e-4, 1e-1),
    "batch_size": tune.choice([32]),
}