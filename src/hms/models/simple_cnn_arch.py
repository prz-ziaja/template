import os
import torch
import tempfile
import pytorch_lightning as pl
import torch.nn as nn
import torch.nn.functional as F
from filelock import FileLock
from torchmetrics import KLDivergence
from torch.utils.data import DataLoader, random_split, Dataset
import numpy as np


class HMSClassifier(pl.LightningModule):
    def __init__(self, config):
        pl.LightningModule.__init__(self)
        self.metric_f = KLDivergence(log_prob=True)
        layer_1_size = config["layer_1_size"]
        layer_2_size = config["layer_2_size"]
        self.lr = config["lr"]

        self.c1 = nn.Conv2d(1,layer_1_size,(3,100),(2,100))
        self.c2 = nn.Conv2d(layer_1_size,layer_2_size,(3,2),(2,1))
        self.c3 = nn.Conv2d(layer_2_size,64,(3,2),(2,1))
        self.d = nn.Dropout(0.1)
        self.m = nn.MaxPool2d((2,40),(2,40))
        self.act = nn.SELU()
        self.l1 = nn.Linear(64*8,128)
        self.l2 = nn.Linear(128,6)

        self.eval_loss = []
        self.eval_kl = []
        self.lossf = nn.KLDivLoss(log_target=True)

    def compute_loss(self, logits, labels):
        return self.lossf(logits, labels)

    def forward(self, x):
        x = self.act(self.c1(x))
        x = self.act(self.c2(x))
        x = self.d(x)
        x = self.act(self.c3(x))
        x = self.d(x)

        x = x.flatten(start_dim=1)
        x = self.act(self.l1(x))
        x = self.d(x)
        x = self.l2(x)
        
        return F.log_softmax(x)

    def training_step(self, train_batch, batch_idx):
        x, y, weight = train_batch
        logits = self.forward(x)
        loss = self.compute_loss(logits, y)
        kl = self.metric_f(logits, y)

        self.log("ptl/train_loss", loss)
        self.log("ptl/train_kl", kl)
        return loss

    def validation_step(self, val_batch, batch_idx):
        x, y, weight = val_batch
        logits = self.forward(x)
        loss = self.compute_loss(logits, y)
        kl = self.metric_f(logits, y)
        self.eval_loss.append(loss)
        self.eval_kl.append(kl)
        return {"val_loss": loss, "val_kl": kl}

    def on_validation_epoch_end(self):
        avg_loss = torch.stack(self.eval_loss).mean()
        avg_kl = torch.stack(self.eval_kl).mean()
        self.log("ptl/val_loss", avg_loss, sync_dist=True)
        self.log("ptl/val_kl", avg_kl, sync_dist=True)
        self.eval_loss.clear()
        self.eval_kl.clear()

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        return optimizer

class HMSDataset(Dataset):
    def __init__(self):
        self.file_path = '/tmp/hms'
        self.files = [x for x in os.listdir(self.file_path) if '_001.npz' in x]
    def __len__(self):
        return len(self.files)
    def __getitem__(self, index):
        output = np.load(self.file_path+f"/{self.files[index]}")
        empty = np.zeros([1,40,400],dtype=np.float32)
        temp_x = output['arr_0'][:40]
        empty[:,:temp_x.shape[0],:] = temp_x
        x = torch.tensor(empty)
        vote_sum = torch.tensor(output['arr_1'].astype(np.float32)).sum()
        y = F.log_softmax(torch.tensor(output['arr_1'],dtype=torch.float32),dim=1)[0]
        return x, y, vote_sum

class HMSDataModule(pl.LightningDataModule):
    def __init__(self, batch_size=32):
        pl.LightningDataModule.__init__(self)
        self.data_dir = tempfile.mkdtemp()
        self.batch_size = batch_size

    def setup(self, stage=None):
        with FileLock(f"{self.data_dir}.lock"):
            hms = HMSDataset()
            self.hms_train, self.hms_val = random_split(hms, [0.8, 0.2])

    def train_dataloader(self):
        return DataLoader(self.hms_train, batch_size=self.batch_size, num_workers=4)

    def val_dataloader(self):
        return DataLoader(self.hms_val, batch_size=self.batch_size, num_workers=4)
