import numpy as np
import pandas as pd
import pyarrow.parquet as pq
import preprocessing.preprocess_functions as pf
import torch

def process(x, dtype=np.float32):
    x = pf.log_42_scaling(x)
    return x.astype(dtype)

def process_inference(x):
    x_processed = process(x["spectrogram"])[:40,1:]
    x["processed"] = x_processed.reshape([1,40,400])
    x['spec_id'] = np.array(list(x['config']['spectrogram_id'].values())[0])
    return x

def process_train(inp):
    config = inp['config']
    spectrogram = inp['spectrogram']
    spectrogram_id = list(config["spectrogram_id"].values())[0]
    samples = pd.DataFrame(config)
    sorted_samples = samples.sort_values('spectrogram_label_offset_seconds')
    times = sorted_samples.spectrogram_label_offset_seconds.to_list()+[float('inf'),]
    
    
    for offset_index in range(len(times)-1):
        lower_boundary = times[offset_index]
        upper_boundary = min(times[offset_index+1], times[offset_index]+300)
        x = spectrogram[(spectrogram[:,0]>=lower_boundary)&(spectrogram[:,0]<upper_boundary)][:,1:]
        if x.shape[0]<15:
            continue
        processed_x = process(x)
        y = sorted_samples[sorted_samples.spectrogram_label_offset_seconds==0][['seizure_vote', 'lpd_vote', 'gpd_vote', 'lrda_vote', 'grda_vote', 'other_vote']].values
        np.savez(f'/tmp/hms/{spectrogram_id}_{int(lower_boundary)}_001.npz', processed_x, y)
    
    return {"res": 1}
