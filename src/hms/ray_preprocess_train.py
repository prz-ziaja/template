import ray
import pandas as pd
import pyarrow.parquet as pq
import numpy as np
import importlib
import json

ray.init(
    _system_config={
        # Allow spilling until the local disk is 99% utilized.
        # This only affects spilling to the local file system.
        "local_fs_capacity_threshold": 0.99,
    },
)


def read_spectrogram(config):
    spectrogram_id = list(config["spectrogram_id"].values())[0]
    spectrogram = pq.read_table(f'../../data/train_spectrograms/{spectrogram_id}.parquet').to_pandas()
    filled_spectrogram = spectrogram.fillna(0).values
    return {"config":config, "spectrogram":filled_spectrogram}


def main(plugin_names):
    assignment = pd.read_csv('../../data/train.csv')
    grouped = assignment[['spectrogram_id','spectrogram_label_offset_seconds', 'seizure_vote', 'lpd_vote', 'gpd_vote', 'lrda_vote', 'grda_vote','other_vote']].groupby(['spectrogram_id', 'seizure_vote', 'lpd_vote', 'gpd_vote', 'lrda_vote', 'grda_vote','other_vote']).min('spectrogram_label_offset_seconds').reset_index()
    ids = grouped.spectrogram_id.unique()
    configs = [grouped[grouped.spectrogram_id == spectrogram_id].to_dict() for spectrogram_id in ids ]
    configs_set = ray.data.from_items([{'config': config} for config in configs])

    plugins = [importlib.import_module(f"preprocessing.{plugin_name}") for plugin_name in plugin_names]

    read_res = configs_set.map(lambda x: read_spectrogram(x['config']))

    for plugin in plugins:
        read_res.map(plugin.process_train).write_parquet("/tmp")


if __name__ == "__main__":
    main(["001_log_scaling"])